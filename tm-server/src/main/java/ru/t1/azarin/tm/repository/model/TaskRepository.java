package ru.t1.azarin.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.azarin.tm.enumerated.Sort;
import ru.t1.azarin.tm.model.Task;

import java.util.List;

@Repository
@Scope("prototype")
public interface TaskRepository extends AbstractUserOwnedRepository<Task> {

    void deleteAllByUserId(@NotNull String userId);

    @Nullable
    List<Task> findAllByUserId(@NotNull String userId);

    @Nullable
    @Query("SELECT t FROM Task t WHERE t.user.id = :userId AND t.project.id = :projectId")
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    @Nullable
    @Query("SELECT t FROM Task t WHERE t.user.id = :userId ORDER BY :sortColumn")
    List<Task> findAllByUserIdWithSort(@Param("userId") String userId, @Param("sortColumn") @NotNull String sortColumn);

    @Nullable
    Task findByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

}
