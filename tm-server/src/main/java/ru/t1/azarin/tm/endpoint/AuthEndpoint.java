package ru.t1.azarin.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.azarin.tm.api.endpoint.IAuthEndpoint;
import ru.t1.azarin.tm.api.service.IAuthService;
import ru.t1.azarin.tm.api.service.dto.IUserDtoService;
import ru.t1.azarin.tm.dto.model.SessionDto;
import ru.t1.azarin.tm.dto.model.UserDto;
import ru.t1.azarin.tm.dto.request.user.UserLoginRequest;
import ru.t1.azarin.tm.dto.request.user.UserLogoutRequest;
import ru.t1.azarin.tm.dto.request.user.UserViewProfileRequest;
import ru.t1.azarin.tm.dto.response.user.UserLoginResponse;
import ru.t1.azarin.tm.dto.response.user.UserLogoutResponse;
import ru.t1.azarin.tm.dto.response.user.UserViewProfileResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.sql.SQLException;

@Controller
@NoArgsConstructor
@AllArgsConstructor
@WebService(endpointInterface = "ru.t1.azarin.tm.api.endpoint.IAuthEndpoint")
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    @NotNull
    @Autowired
    private IAuthService authService;

    @NotNull
    @Autowired
    private IUserDtoService userServiceDTO;

    @NotNull
    @Override
    @WebMethod
    public UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLoginRequest request
    ) throws SQLException {
        @NotNull final String token = authService.login(request.getLogin(), request.getPassword());
        return new UserLoginResponse(token);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLogoutRequest request
    ) {
        check(request);
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserViewProfileResponse viewProfile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserViewProfileRequest request
    ) {
        @NotNull final SessionDto session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final UserDto user = userServiceDTO.findOneById(userId);
        return new UserViewProfileResponse(user);
    }

}
