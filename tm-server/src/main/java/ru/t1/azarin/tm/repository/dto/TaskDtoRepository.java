package ru.t1.azarin.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.azarin.tm.dto.model.TaskDto;
import ru.t1.azarin.tm.enumerated.Sort;

import java.util.List;

@Repository
@Scope("prototype")
public interface TaskDtoRepository extends AbstractUserOwnedDtoRepository<TaskDto> {

    void deleteAllByUserId(@NotNull String userId);

    @Nullable
    List<TaskDto> findAllByUserId(@NotNull String userId);

    @Query("SELECT t FROM TaskDto t WHERE t.userId = :userId AND t.projectId = :projectId")
    List<TaskDto> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

    @Query("SELECT t FROM TaskDto t WHERE t.userId = :userId ORDER BY :sortColumn")
    List<TaskDto> findAllByUserIdWithSort(@Param("userId") String userId, @Param("sortColumn") @NotNull String sortColumn);

    @Nullable
    TaskDto findByUserIdAndId(@NotNull String userId, @NotNull String id);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

}
