package ru.t1.dazarin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.t1.dazarin.tm.model.dto.TaskDto;

import java.util.List;

@RequestMapping("/api/task")
public interface ITaskEndpoint {

    @GetMapping(value = "/findAll")
    List<TaskDto> findAll();

    @GetMapping(value = "/findById/{id}")
    TaskDto findById(@NotNull @PathVariable("id") String id);

    @PostMapping(value = "/create")
    TaskDto create();

    @DeleteMapping(value = "/deleteById/{id}")
    void deleteById(@NotNull @PathVariable("id") String id);

    @DeleteMapping(value = "/deleteAll")
    void deleteAll();

    @PutMapping(value = "/update")
    TaskDto update(@NotNull @RequestBody TaskDto taskDto);

}
