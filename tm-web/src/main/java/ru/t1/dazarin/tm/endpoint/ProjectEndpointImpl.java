package ru.t1.dazarin.tm.endpoint;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.t1.dazarin.tm.api.endpoint.IProjectEndpoint;
import ru.t1.dazarin.tm.model.dto.ProjectDto;
import ru.t1.dazarin.tm.service.dto.ProjectDtoService;

import java.util.List;

@RestController
@RequestMapping("/api/project")
@RequiredArgsConstructor
public class ProjectEndpointImpl implements IProjectEndpoint {

    private final ProjectDtoService projectDtoService;

    @Override
    @GetMapping(value = "/findAll")
    public List<ProjectDto> findAll() {
        return projectDtoService.findAll();
    }

    @Override
    @GetMapping(value = "/findById/{id}")
    public ProjectDto findById(@NotNull @PathVariable("id") final String id) {
        return projectDtoService.findById(id);
    }

    @Override
    @PostMapping(value = "/create")
    public ProjectDto create() {
        return projectDtoService.create();
    }

    @Override
    @DeleteMapping(value = "/deleteById/{id}")
    public void deleteById(@NotNull @PathVariable("id") final String id) {
        projectDtoService.deleteById(id);
    }

    @Override
    @DeleteMapping(value = "/deleteAll")
    public void deleteAll() {
        projectDtoService.deleteAll();
    }

    @Override
    @PutMapping(value = "/update")
    public ProjectDto update(@NotNull @RequestBody final ProjectDto projectDto) {
        return projectDtoService.save(projectDto);
    }

}
