package ru.t1.dazarin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.t1.dazarin.tm.model.dto.ProjectDto;

import java.util.List;

@RequestMapping("/api/project")
public interface IProjectEndpoint {

    @GetMapping(value = "/findAll")
    List<ProjectDto> findAll();

    @GetMapping(value = "/findById/{id}")
    ProjectDto findById(@NotNull @PathVariable("id") String id);

    @PostMapping(value = "/create")
    ProjectDto create();

    @DeleteMapping(value = "/deleteById/{id}")
    void deleteById(@NotNull @PathVariable("id") String id);

    @DeleteMapping(value = "/deleteAll")
    void deleteAll();

    @PutMapping(value = "/update")
    ProjectDto update(@NotNull @RequestBody ProjectDto projectDto);

}
