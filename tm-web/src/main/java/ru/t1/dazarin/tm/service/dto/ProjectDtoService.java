package ru.t1.dazarin.tm.service.dto;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.dazarin.tm.api.repository.IProjectDtoRepository;
import ru.t1.dazarin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.dazarin.tm.model.dto.ProjectDto;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProjectDtoService {

    private final IProjectDtoRepository projectDtoRepository;

    public List<ProjectDto> findAll() {
        return projectDtoRepository.findAll();
    }

    @Nullable
    public ProjectDto findById(@NotNull final String id) {
        return projectDtoRepository.findById(id).orElse(null);
    }

    public ProjectDto create() {
        @NotNull final ProjectDto projectDto = new ProjectDto();
        projectDto.setName("Project Name " + System.currentTimeMillis());
        projectDto.setDescription("Project Description");
        return projectDtoRepository.saveAndFlush(projectDto);
    }

    public void deleteById(@NotNull final String projectDtoId) {
        if (!projectDtoRepository.existsById(projectDtoId)) throw new ProjectNotFoundException();
        projectDtoRepository.deleteById(projectDtoId);
    }

    public void deleteAll() {
        projectDtoRepository.deleteAll();
    }

    public ProjectDto save(@NotNull final ProjectDto projectDto) {
        return projectDtoRepository.save(projectDto);
    }

}