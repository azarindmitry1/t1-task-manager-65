package ru.t1.azarin.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.azarin.tm.dto.model.TaskDto;
import ru.t1.azarin.tm.dto.request.task.TaskListByProjectIdRequest;
import ru.t1.azarin.tm.event.ConsoleEvent;
import ru.t1.azarin.tm.util.TerminalUtil;

import java.util.List;

@Component
public final class TaskListByProjectIdListener extends AbstractTaskListener {

    @NotNull
    public final static String NAME = "task-show-by-project-id";

    @NotNull
    public final static String DESCRIPTION = "Show task by project id.";

    @Override
    @EventListener(condition = "@taskListByProjectIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String id = TerminalUtil.nextLine();

        @NotNull final TaskListByProjectIdRequest request = new TaskListByProjectIdRequest(getToken());
        request.setId(id);
        @NotNull final List<TaskDto> tasks = taskEndpoint.listTaskByProjectIdResponse(request).getTasks();
        renderTask(tasks);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
